package excel

import (
	"fmt"
	"strconv"

	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/go-gota/gota/dataframe"
)

// type Excel struct {

// }

type DataFrame dataframe.DataFrame

func NewExcel(filename string) {
	xlsx := excelize.NewFile()
	err := xlsx.SaveAs(filename)
	if err != nil {
		panic(err)
	}
}

func LoadMaps(maps []map[string]interface{}, options ...dataframe.LoadOption) DataFrame{
	result := dataframe.LoadMaps(maps,options...)
	return (DataFrame)(result)
}


func (df DataFrame) Concat(dfb DataFrame) DataFrame{
	dfc := (dataframe.DataFrame)(df).Concat((dataframe.DataFrame)(dfb))
	return (DataFrame)(dfc)
}


func CalcExcelCell(r ,c int64) string{
	b := strconv.FormatInt(c,26)
	sz := len(b)
	result := make([]byte,sz)
	for i :=0;i<sz;i++{
		d,_ := strconv.ParseInt(string(b[i]),26,0)
		result[i] = byte(d) + 'A' - 1
	}
	result[sz-1] += 1
	return fmt.Sprintf("%s%d",string(result),r+1)
}

func (df DataFrame) ToExcel(filename string) {
	// columns := (dataframe.DataFrame)(df).Names()

	xlsx := excelize.NewFile()
	// sheetIndex := xlsx.NewSheet("Data")
	sheetIndex := xlsx.GetActiveSheetIndex()
	// xlsx.SetActiveSheet(sheetIndex)
	sheetName := xlsx.GetSheetName(sheetIndex)

	for r,record := range (dataframe.DataFrame)(df).Records() {
		// fmt.Println(record)
		for c,rec := range(record){
			// fmt.Println(rec)
			xlsx.SetCellValue(sheetName,CalcExcelCell(int64(r),int64(c)),rec)
		}

	}

	// // 插入header
	// for i,rec := range(columns){
	// 	xlsx.SetCellValue("Data",calcExcelCell(int64(0),int64(i)),rec)
	// }

	err := xlsx.SaveAs(filename)
	if err != nil {
		panic(err)
	}
}
